package io;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import sbp.io.MyIOExample;

import java.io.*;
import java.nio.file.*;



/**
 * Класс с тестами для {@link MyIOExample}
 */

public class MyIOExampleTest {
    public MyIOExample testExample = new MyIOExample();
    public String sourceFileName = "test/test/test.txt";
    public String destinationFileName = "test/test/copyTest.txt";
    public Path source = Paths.get(destinationFileName);
    public Path destination = Paths.get(destinationFileName);

    /**Проверка создание файла в методе .workWithFile()*/
    @Test
    public void workWithFile_Test() {
        boolean result = testExample.workWithFile(sourceFileName);
        Assertions.assertEquals(result, Files.exists(Paths.get(sourceFileName)));
    }

    /**Проверка результатов копирования методом .copyFile()*/
    @Test
    public void copyFile_Test() {
        if (Files.exists(destination)) {
            try {
                Files.delete(destination);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Path directory = source.toAbsolutePath().getParent();
        if (directory != null) {
            try {
                Files.createDirectories(directory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!Files.exists(source)) {
            try {
                Files.createFile(source);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        try (BufferedWriter writer = Files.newBufferedWriter(source)) {
            writer.write("Some test text");
        } catch (IOException e) {
            e.printStackTrace();
        }

        testExample.copyFile(sourceFileName, destinationFileName);

        try (BufferedReader readerSource = Files.newBufferedReader(source); BufferedReader readerDestination = Files.newBufferedReader(destination);) {
            String line;
            while ((line = readerSource.readLine()) != null) {
                Assertions.assertEquals(line, readerDestination.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**Проверка результатов копирования методом .copyBufferedFile()*/
    @Test
    public void copyBufferedFile_Test() {
        if (Files.exists(destination)) {
            try {
                Files.delete(destination);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Path directory = source.toAbsolutePath().getParent();
        if (directory != null) {
            try {
                Files.createDirectories(directory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!Files.exists(source)) {
            try {
                Files.createFile(source);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        try (BufferedWriter writer = Files.newBufferedWriter(source)) {
            writer.write("Some test text");
        } catch (IOException e) {
            e.printStackTrace();
        }

        testExample.copyBufferedFile(sourceFileName, destinationFileName);

        try (BufferedReader readerSource = Files.newBufferedReader(source); BufferedReader readerDestination = Files.newBufferedReader(destination);) {
            String line;
            while ((line = readerSource.readLine()) != null) {
                Assertions.assertEquals(line, readerDestination.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**Проверка результатов копирования методом .copyFileWithReaderAndWriter()*/
    @Test
    public void copyFileWithReaderAndWriter_Test() {
        if (Files.exists(destination)) {
            try {
                Files.delete(destination);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        Path directory = source.toAbsolutePath().getParent();
        if (directory != null) {
            try {
                Files.createDirectories(directory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!Files.exists(source)) {
            try {
                Files.createFile(source);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        try (BufferedWriter writer = Files.newBufferedWriter(source)) {
            writer.write("Some test text");
        } catch (IOException e) {
            e.printStackTrace();
        }

        testExample.copyFileWithReaderAndWriter(sourceFileName, destinationFileName);

        try (BufferedReader readerSource = Files.newBufferedReader(source); BufferedReader readerDestination = Files.newBufferedReader(destination);) {
            String line;
            while ((line = readerSource.readLine()) != null) {
                Assertions.assertEquals(line, readerDestination.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}