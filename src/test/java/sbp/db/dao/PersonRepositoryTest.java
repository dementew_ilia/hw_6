package sbp.db.dao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.io.MyIOExample;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс с тестами для {@link PersonRepository}
 */
class PersonRepositoryTest
{
    public String databasePath = "/home/ilia/Программирование/Java/Сбер/HW_6/testBase.db";
    public Path connectionPath = Paths.get(databasePath);
    public String jdbcURL = "jdbc:sqlite:" + databasePath;

    /**
     * Проверка наличия БД
     */
    @Test
    void connectionPath_Test()
    {
        Assertions.assertTrue(Files.exists(connectionPath));
    }

    /**
     * Проверка создания таблицы
     */
    @Test
    void createTable_Test()
    {
        try (Connection connection = DriverManager.getConnection(jdbcURL))
        {
            PersonRepository repository = new PersonRepository(connection);
            String tableName = "Person";

            repository.createTable(tableName);

            try (Statement statement = connection.createStatement())
            {
                String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + tableName + "'";
                try (ResultSet resultSet = statement.executeQuery(sql))
                {
                    resultSet.next();
                    Assertions.assertEquals(resultSet.getString("name"), tableName);
                }
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Проверка удаления таблицы
     * */
    @Test
    void dropTable_Test()
    {
        try (Connection connection = DriverManager.getConnection(jdbcURL))
        {
            PersonRepository repository = new PersonRepository(connection);
            String tableName = "Person";

            repository.dropTable(tableName);
            try (Statement statement = connection.createStatement())
            {
                String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + tableName + "'";
                try (ResultSet resultSet = statement.executeQuery(sql)){
                    boolean result = resultSet.next();

                    Assertions.assertFalse(result);
                }
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Проверка добавления строк в таблицу*/
    @Test
    void addPerson_Test()
    {
        Map<String, Integer> personsAge = new LinkedHashMap<>();
        personsAge.put("Ivan", 20);
        personsAge.put("Olga", 25);
        personsAge.put("Inna", 32);
        personsAge.put("Oleg", 12);
        personsAge.put("Pavel", 48);

        try (Connection connection = DriverManager.getConnection(jdbcURL))
        {
            PersonRepository repository = new PersonRepository(connection);
            String tableName = "Person";

            repository.createTable(tableName);

            int personId = 1;
            for (Map.Entry<String, Integer> entry : personsAge.entrySet())
            {
                Person person = new Person(personId, entry.getKey(), entry.getValue());
                repository.addPerson(tableName, person);
                personId++;
            }

            try (Statement statement = connection.createStatement())
            {
                String sql = "SELECT name FROM " + tableName + " where id = 1";
                try (ResultSet resultSet = statement.executeQuery(sql)) {
                    resultSet.next();

                    Assertions.assertEquals(resultSet.getString("name"), "Ivan");
                }
            }

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**Проверка изменения значения в поле возраст*/
    @Test
    void changeAgePerson_Test()
    {
        try (Connection connection = DriverManager.getConnection(jdbcURL))
        {
            PersonRepository repository = new PersonRepository(connection);
            String tableName = "Person";
            int newAge = 50;

            repository.changeAgePerson(tableName, 1, newAge);

            try (Statement statement = connection.createStatement()){
                String sql = "SELECT age FROM " + tableName + " where id = 1";
                try (ResultSet resultSet = statement.executeQuery(sql)){
                    resultSet.next();
                    Assertions.assertEquals(Integer.valueOf(resultSet.getString("age")), newAge);
                }
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}