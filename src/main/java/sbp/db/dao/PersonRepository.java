package sbp.db.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * класс для работы сщ структорой хранения "Person"
 * */
public class PersonRepository {
    private final Connection connection;

    /**
     * Конструктор для создания соединения с БД
     * @param  connection - конфигурация соединения
     * */
    public PersonRepository(Connection connection){
        this.connection = connection;
    }

    /**
     * Создать таблицу
     * */
    public void  createTable(String tableName){
        if(tableName == null){
            throw new IllegalArgumentException("arguments of createTable() are null");
        }

        try (Statement statement = connection.createStatement()){
            String sql = "create table " +
                    "\"" + tableName + "\""+
                    "(id integer," +
                    "name varchar(32)," +
                    "age integer" +
                    ");";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Удалить таблицу
     * */
    public void  dropTable(String tableName){
        if(tableName == null){
            throw new IllegalArgumentException("arguments of createTable() are null");
        }


        try(Statement statement = connection.createStatement()) {
            String sql = "drop table " + "\"" + tableName + "\";";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Изменить значение в поле "age"
     * */
    public  void  changeAgePerson(String tableName, int id, int newAge){


        try (Statement statement = connection.createStatement()){
            statement.executeUpdate("update \"" + tableName  + "\" set age = '" + newAge + "' where id ='" + id + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

/**
 * Добавить запись в таблицу
 * */
    public void addPerson(String tableName, Person person){

        try (Statement statement = connection.createStatement()){
            String sql = String.format("insert into '%s' ('id', 'name', 'age') VALUES ('%d', '%s', '%d')",
                    tableName, person.getId(), person.getName(), person.getAge());
            boolean hasResultSet = statement.execute(sql);
            statement.getUpdateCount();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
