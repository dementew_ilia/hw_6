package sbp.io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class MyIOExample
{
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     * - абсолютный путь
     * - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     * - размер
     * - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     *
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName)
    {
        Path path = Paths.get(fileName);
        System.out.println("path= " + path);

        Path directory = path.toAbsolutePath().getParent();
        System.out.println("directory= " + directory);

        if (directory != null)
        {
            try
            {
                Files.createDirectories(directory);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        if (!Files.exists(path))
        {
            try
            {
                Files.createFile(path);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
        else
        {
            System.out.println("absolute path: " + path.toAbsolutePath().normalize());
            System.out.println("parent directory name: " + "\"" + path.getParent().getFileName() + "\"");
        }

        System.out.println("path is regular file - " + Files.isRegularFile(path));

        if (Files.isRegularFile(path))
        {
            try
            {
                System.out.println("file size: " + Files.size(path));
                System.out.println("last modified time: " + Files.getLastModifiedTime(path));
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }

        return Files.exists(path);
    }


    /**
     * Метод по проверке наличия файла и директории для копирования
     * @param sourceFileName -имя пути для файла источника
     * @param destinationFileName -имя пути для файла копии
     * */
    private boolean checkingFilesAndDirectoriesToCopy(String sourceFileName, String destinationFileName)
    {
        if (!Files.exists(Paths.get(sourceFileName)))
        {
            return true;
        }

        Path destinationDirectory = Paths.get(destinationFileName).toAbsolutePath().getParent();
        if (destinationDirectory != null)
        {
            try
            {
                Files.createDirectories(destinationDirectory);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName)
    {
        if (checkingFilesAndDirectoriesToCopy(sourceFileName, destinationFileName)) return false;

        try (FileInputStream fileInputStream = new FileInputStream(sourceFileName);
             FileOutputStream fileOutputStream = new FileOutputStream(destinationFileName))
        {
            int i;
            while ((i = fileInputStream.read()) != -1)
            {
                fileOutputStream.write((char) i);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return true;
    }


    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) {
        if (checkingFilesAndDirectoriesToCopy(sourceFileName, destinationFileName)) return false;

        Path source = Paths.get(sourceFileName);
        Path destination = Paths.get(destinationFileName);

        try (BufferedReader bufferedReader = Files.newBufferedReader(source);
             BufferedWriter bufferedWriter = Files.newBufferedWriter(destination))
        {
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                bufferedWriter.write(line);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) {
        if (checkingFilesAndDirectoriesToCopy(sourceFileName, destinationFileName)) return false;

        try (FileReader fr = new FileReader(sourceFileName);
             FileWriter writer = new FileWriter(destinationFileName))
        {
            int i;
            while ((i = fr.read()) != -1)
            {
                writer.write((char) i);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return true;
    }
}
